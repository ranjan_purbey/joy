# README #

Joy aims to allow android devices run customizable voice commands offline using pocketsphinx STT engine.

###Very Short HowTo###
Download and install the "Joy.apk" file on your android device.

###Short HowTo###
Download the repo. Download and setup eclipse IDE alongwith Android SDK and NDK. Import the downloaded repo as an android project.

###Long HowTo###
[wait, 'coz I got to prepare for my exams.]

##Usage##
Launch the app and wait for the recognizer to setup. By default, it detects only digits. To add/edit the commands, click "Edit Vocabulary".

##Next Stops##
* Voice keyboard
* Virtual assistant
* IoT API