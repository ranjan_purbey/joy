
//handle the edit text activity
//move data to assets
package org.roboism.joy;

import java.io.File;
import java.io.PrintWriter;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;
import edu.cmu.pocketsphinx.Assets;
import edu.cmu.pocketsphinx.Hypothesis;
import edu.cmu.pocketsphinx.RecognitionListener;
import edu.cmu.pocketsphinx.SpeechRecognizer;
import edu.cmu.pocketsphinx.SpeechRecognizerSetup;
public class MainActivity extends Activity {
ToggleButton listenBtn;
Button editDictBtn;
TextView hypothesisFld;
SharedPreferences prefs;
SpeechRecognizer recognizer;
RecognitionListener listener;
String defaultDict = "one two three four five six seven eight nine zero";
File filesDir, dictFile, ngramFile;
static final byte REQUEST_CODE = 4;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		filesDir = getExternalFilesDir(null);
		dictFile = new File(filesDir, "dict.txt");
		ngramFile = new File(filesDir, "custom_EN.lm");
		listenBtn = (ToggleButton)findViewById(R.id.listen_btn);
		editDictBtn = (Button)findViewById(R.id.edit_dict_btn);
		hypothesisFld = (TextView)findViewById(R.id.hypothesis_fld);
		prefs = getPreferences(MODE_PRIVATE);
		boolean reset =false;
		if(prefs.getBoolean("firstRun", true)){
			reset = true;
			prefs.edit().putBoolean("firstRun", false).commit();
		}
		setupSTT(reset);
		
	}
	private void setupSTT(final boolean reset){
		new AsyncTask<Void, Void, Boolean>() {
		@Override
		protected void onPreExecute(){
			listenBtn.setEnabled(false);
		}
		@Override
		protected Boolean doInBackground(Void...params){
			try{
				new Assets(getApplicationContext(), filesDir.getPath()).syncAssets();
				recognizer = SpeechRecognizerSetup
						.defaultSetup()
						.setAcousticModel(new File(filesDir,"en-us-ptm"))
						.setDictionary(new File(filesDir, "cmudict-en-us.dict"))
						.getRecognizer();

				
				updateDict(reset);
				recognizer.addNgramSearch("everything", ngramFile);
				return true;
			}
			catch(Exception e){
				Log.e("joy", e.toString());
				return false;
			}
		}
		@Override
		protected void onPostExecute(Boolean result){
			if(result)recognizer.addListener(listener = new RecognitionListener() {
				
				@Override
				public void onTimeout() {
					// TODO Auto-generated method stub
					
				}
				
				@Override
				public void onResult(Hypothesis arg0) {
					// TODO Auto-generated method stub
					if(arg0  != null)hypothesisFld.setText(arg0.getHypstr());
				}
				
				@Override
				public void onPartialResult(Hypothesis arg0) {
					// TODO Auto-generated method stub
					if(arg0 == null)return;
					hypothesisFld.setText(arg0.getHypstr());
				}
				
				@Override
				public void onError(Exception arg0) {
					// TODO Auto-generated method stub
					hypothesisFld.setText(arg0.toString());
				}
				
				@Override
				public void onEndOfSpeech() {
					// TODO Auto-generated method stub
					
				}
				
				@Override
				public void onBeginningOfSpeech() {
					// TODO Auto-generated method stub
					
				}
			});
			else hypothesisFld.setText("Failed to initiate the recogniser");
			listenBtn.setEnabled(result);
			listenBtn.setChecked(false);
		}
	}.execute();
}
	private void updateDict(boolean reset){
		try {
			if(!dictFile.exists()){
				
				dictFile.createNewFile();
				PrintWriter out = new PrintWriter(dictFile);
				out.print(defaultDict);
				out.close();
			}
			if(reset || !ngramFile.exists())kylm.main.CountNgrams.main(new String[]{dictFile.getAbsolutePath(), ngramFile.getAbsolutePath()});
			
			
		}catch (Exception e) {
			// TODO Auto-generated catch block
			Log.e("joy", e.toString());
		}
	}
	@Override
	protected void onResume(){
		super.onResume();
		if(!listenBtn.isEnabled())listenBtn.setText(R.string.wait);
		hypothesisFld.setMovementMethod(new ScrollingMovementMethod());
		editDictBtn.setOnClickListener(new Button.OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				startActivityForResult(new Intent(getApplicationContext(),DictionaryEditor.class).putExtra("dictFile", MainActivity.this.dictFile).putExtra("defaultDict", MainActivity.this.defaultDict), REQUEST_CODE);
			}
		});
		listenBtn.setOnCheckedChangeListener(new ToggleButton.OnCheckedChangeListener() {
			
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				// TODO Auto-generated method stub
				if(isChecked){
					hypothesisFld.setText("");
					recognizer.startListening("everything");
				}
				else new AsyncTask<Void, Void, Void>(){
					@Override
					protected Void doInBackground(Void... params) {
						// TODO Auto-generated method stub
						recognizer.stop();
						return null;
					}
				}.execute();
			}
		});
		
	}
	@Override
	public void onDestroy(){
		super.onDestroy();
		if(recognizer!=null){recognizer.cancel();recognizer.shutdown();}
	}
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent intent){
		super.onActivityResult(requestCode, resultCode, intent);
		if(requestCode == REQUEST_CODE && (Boolean)intent.getExtras().get("updated"))setupSTT((Boolean)intent.getExtras().get("updated"));
		
	}
}