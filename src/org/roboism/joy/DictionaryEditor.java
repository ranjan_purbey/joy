package org.roboism.joy;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Scanner;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.Toast;

public class DictionaryEditor extends Activity {
EditText dictFld;
MenuItem saveItem, restoreItem;
File dictFile;
String defaultDict, currentDict;
boolean updated;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_dictionary_editor);
		dictFld = (EditText)findViewById(R.id.dict_fld);
		dictFile = (File)getIntent().getExtras().get("dictFile");
		defaultDict = (String)getIntent().getExtras().get("defaultDict");
		updated = false;
		try {
			Scanner s= new Scanner(dictFile, "UTF-8");
			currentDict = s.useDelimiter("\\A").next();
			s.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			Log.e("joy", e.getMessage());
			currentDict = defaultDict;
		} catch(Exception e){
			Log.e("joy", e.getMessage());
		}
		dictFld.setText(currentDict);
		
	}
	@Override
		public boolean onCreateOptionsMenu(Menu menu) {
			// TODO Auto-generated method stub
			super.onCreateOptionsMenu(menu);
			getMenuInflater().inflate(R.menu.dictionary_menu, menu);
			saveItem = (MenuItem)menu.findItem(R.id.save_item);
			restoreItem = (MenuItem)menu.findItem(R.id.restore_item);
			saveItem.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
				
				@Override
				public boolean onMenuItemClick(MenuItem item) {
					// TODO Auto-generated method stub
					PrintWriter out;
					try {
						out = new PrintWriter(dictFile);
						currentDict = dictFld.getText().toString();
						out.print(currentDict);
						out.close();
						updated = true;
						Toast.makeText(getApplicationContext(), R.string.dictUpdated, Toast.LENGTH_SHORT).show();
					} catch (FileNotFoundException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					return false;
				}
			});
			restoreItem.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
				
				@Override
				public boolean onMenuItemClick(MenuItem item) {
					// TODO Auto-generated method stub
					dictFld.setText(currentDict);
					return false;
				}
			});
			return true;
		}
	@Override
	public void onBackPressed(){
		
		Intent intent = new Intent();
		intent.putExtra("updated", updated);
		setResult(Activity.RESULT_OK, intent);
		super.onBackPressed();
	}
}
